# Bigit

Script to show basic infomation about changed git repos in a directory.

Runs `git status -s` and `git --no-pager diff --stat` in behind. Output is formatted for better visibility.

## Installation

Save the script at `~/.local/bin`

or run

```
curl -o ~/.local/bin/bigit https://gitlab.com/kannanvm/bigit/-/raw/main/bigit && chmod u+x ~/.local/bin/bigit
```
